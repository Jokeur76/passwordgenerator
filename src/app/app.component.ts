import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Password Generator';
  private chars;
  public compo;
  public count = 0;
  public lastPassword = '';

  constructor() {
    this.raz();
  }

  generatePassword() {
    let pass = '';
    let lengthPass = this.compo.voyelles.nbr + this.compo.consonnes.nbr + this.compo.chiffres.nbr + this.compo.caracteres_speciaux.nbr;
    // get voyelle
    this.shuffle(this.chars.Voyelles);
    pass += this.chars.Voyelles.slice(0, this.compo.voyelles.nbr).join('');
    // get Consonnes
    this.shuffle(this.chars.Consonnes);
    pass += this.chars.Consonnes.slice(0, this.compo.consonnes.nbr).join('');
    // get Chiffres
    this.shuffle(this.chars.Chiffres);
    pass += this.chars.Chiffres.slice(0, this.compo.chiffres.nbr).join('');
    // get Caracteres_speciaux
    this.shuffle(this.chars.Caracteres_speciaux);
    pass += this.chars.Caracteres_speciaux.slice(0, this.compo.caracteres_speciaux.nbr).join('');
    this.lastPassword = pass;
  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  raz() {
    this.chars = {};
    this.chars.Voyelles = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'Y', 'O', 'U', 'Y'];
    this.chars.Consonnes = ['z', 'r', 't', 'p', 'm', 'l', 'k', 'j', 'h', 'g', 'f', 'd', 's', 'q', 'w', 'x', 'c', 'v', 'b', 'n', 'Z', 'R', 'T', 'P', 'M', 'L', 'K', 'J', 'H', 'G', 'F', 'D', 'S', 'Q', 'W', 'X', 'C', 'V', 'B', 'N'];
    this.chars.Chiffres = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    this.chars.Caracteres_speciaux = ['!', '#', '$', '%', '&', '*', '+', '-', '/', '=', '?', '^', '_', '{', '|', '}'];
    this.compo = {
      voyelles: {
        nbr: 0,
        selected: false
      },
      consonnes: {
        nbr: 0,
        selected: false
      },
      chiffres: {
        nbr: 0,
        selected: false
      },
      caracteres_speciaux: {
        nbr: 0,
        selected: false
      }
    };
    this.lastPassword = '';
  }

  addCharType(type) {
    switch (type) {
      case 'voyelles':
        this.compo.voyelles.selected = true;
        break;
      case 'consonnes':
        this.compo.consonnes.selected = true;
        break;
      case 'chiffres':
        this.compo.chiffres.selected = true;
        break;
      case 'caracteres_speciaux':
        this.compo.caracteres_speciaux.selected = true;
        break;
    }
  }

  getRandom(start, end) {
    return Math.floor((Math.random() * end) + start);
  }
}
